﻿using market.Models;
using market.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace market.Controllers
{
    public class OrdersController : Controller
    {
        private marketContext db = new marketContext();

        // GET: Orders
        public ActionResult NewOrder()
        {
            
            var orderView = new OrderView();
            orderView.Customer = new Customer();
            orderView.Products = new List<ProductOrder>();

            var list = db.Customers.ToList(); // vai buscar todos os clientes
            ViewBag.CustomerID = new SelectList(list.OrderBy(c => c.Name).ToList(),"CustomerID","Name");

            return View(orderView);
        }
    }
}