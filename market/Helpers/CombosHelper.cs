﻿using market.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace market.Helpers
{
    public class CombosHelper:IDisposable
    {
        private static marketContext db = new marketContext(); // abre o datacontext

        public static List<DocumentType> GetDocumentTypes() // retorna a lista dos tipos de documentos
        {
            var DocumentTypes = db.DocumentTypes.ToList();
            DocumentTypes.Add(new DocumentType
            {
                // criar documento novo
                DocumentTypeID = 0,
                Description = "(Selecione um tipo de documento...)"
            });

            return DocumentTypes.OrderBy(d => d.Description).ToList();
        }

        public void Dispose()
        {
            db.Dispose(); // fecha o datacontext
        }
    }
}