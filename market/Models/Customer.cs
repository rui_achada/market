﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace market.ViewModels
{
    public class Customer
    {
        [Key]
        public int CustomerID { get; set; }

        [StringLength(30, ErrorMessage = "O campo {0} deverá conter entre {2} a {1} caracteres", MinimumLength = 3)]
        [Required(ErrorMessage ="Tem que inserir..")]
        [Display(Name ="Primeiro Nome")]
        public string FirstName { get; set; }

        [StringLength(30, ErrorMessage = "O campo {0} deverá conter entre {2} a {1} caracteres", MinimumLength = 3)]
        [Required(ErrorMessage = "Tem que inserir..")]
        public string LastName { get; set; }

        [Display(Name ="Nome Completo")]
        [NotMapped]
        public string Name { get { return $"{FirstName} {LastName}"; } }

        public string Phone { get; set; }

        public string Address { get; set; }

        public string Email { get; set; }

        [Display(Name ="Nº Documento")]
        public string Document { get; set; }

        [Required(ErrorMessage ="O documento é obrigatório")]
        [Range(1, double.MaxValue, ErrorMessage ="Tem que selecionar um documento")]
        public int DocumentTypeID { get; set; }

        public virtual DocumentType DocumentType { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
    }
}