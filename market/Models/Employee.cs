﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace market.ViewModels
{
    public class Employee
    {
        [Key]
        public int EmployeeID { get; set; }

        [Display(Name = "Primeiro Nome")]
        public string FirstName { get; set; }

        [Display(Name = "Apelido")]
        public string LastName { get; set; }

        [Display(Name = "Salário")]
        [Required(ErrorMessage ="Tem que inserir um valor para o {0}")]
        public decimal Salary { get; set; }

        [Display(Name = "Percentagem de bónus")]
        [Range(0,20,ErrorMessage ="O valor da {0} deverá ser entre {1} a {2}")]
        public float BonusPercent { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Data de nascimento")]
        [Required(ErrorMessage = "Tem que inserir uma {0}")]
        public DateTime DateOfBirth { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Data início trabalho")]
        public DateTime StartTime { get; set; }

        public string Email { get; set; }

        public string URL { get; set; }

        [Display(Name = "Documento")]
        [Required(ErrorMessage = "Tem que inserir um {0}")]
        public int DocumentTypeID { get; set; } // vai ser usado para guardar o ID do documento (relaciona com o outro modelo DocumentType.cs)

        [NotMapped]
        public int Age
        { get
            {
                var myAge = DateTime.Now.Year - DateOfBirth.Year;
                if(DateOfBirth > DateTime.Now.AddYears(-myAge)) // maior ou menor?
                {
                    myAge--;
                }

                return myAge;
            }
        }

        public virtual DocumentType DocumentType { get; set; } // vai efetuar a ligação os tipos de documentos da outra tabela/modelo (é uma propriedade virtual)
    }
}