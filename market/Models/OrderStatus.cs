﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace market.ViewModels
{
    public enum OrderStatus
    {
        Created,
        InProgress,
        Shipped,
        Delivered
    }
}