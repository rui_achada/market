﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace market.ViewModels
{
    public class Product
    {
        [Key]
        public int ProductID { get; set; }

        [Display(Name ="Descrição")]
        [StringLength(30,ErrorMessage ="A {0} deve ter entre {2} a {1} caracteres",MinimumLength =3)]
        [Required(ErrorMessage ="Deve inserir uma {0}")]
        public string Description { get; set; }

        [DataType(DataType.Currency)] // tipo de dados (moeda)
        [DisplayFormat(DataFormatString ="{0:C2}",ApplyFormatInEditMode =false)] // com duas casas decimais. ApplyFormat false significa que ele vai guardar com o formato que está na tabela e não força este formato
        [Display(Name ="Preço")]
        [Required(ErrorMessage ="Deve inserir um {0}")]
        public decimal Price { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:dd/MM/yyyy}", ApplyFormatInEditMode =true)]
        [Display(Name = "Ultima compra")]
        public DateTime LastBuy { get; set; }

        [DataType(DataType.Currency)] // metemos como moeda só para ter casas decimais
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = false)] // com duas casas decimais. ApplyFormat false significa que ele vai guardar com o formato que está na tabela e não força este formato
        public float Stock { get; set; }

        [DataType(DataType.MultilineText)]
        public string Remarks { get; set; }

        public virtual ICollection<SupplierProduct> SupplierProducts { get; set; }

        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}