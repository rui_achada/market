﻿using System.ComponentModel.DataAnnotations;

namespace market.Models
{
    public class ProductOrder : ViewModels.Product
    {
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = false)]
        [Display(Name = "Quantidade")]
        [Required(ErrorMessage = "Deve inserir a {0}")]
        public float Quantity { get; set; }

        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = false)]
        [Display(Name = "Valor")]
        [Required(ErrorMessage = "Deve inserir o {0}")]
        public decimal Value { get { return Price * (decimal)Quantity; } }
    }
}