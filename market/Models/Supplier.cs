﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace market.ViewModels
{
    public class Supplier
    {
        [Key]
        public int SupplierID { get; set; }

        [Required(ErrorMessage ="Tem que introduzir um nome {0} para o Fornecedor")]
        [StringLength(30,ErrorMessage ="O campo {0} deverá conter entre {2} a {1} caracteres",MinimumLength =3)]
        [Display(Name = "Nome")]
        public string Name { get; set; }

        [Display(Name="Nome do contacto")]
        [Required(ErrorMessage = "Tem que introduzir um {0} para o Fornecedor")]
        [StringLength(30, ErrorMessage = "O campo {0} deverá conter entre {2} a {1} caracteres", MinimumLength = 3)]
        public string ContactFirstName { get; set; }

        [Display(Name = "Apelido do contacto")]
        [Required(ErrorMessage = "Tem que introduzir um {0} para o Fornecedor")]
        [StringLength(30, ErrorMessage = "O campo {0} deverá conter entre {2} a {1} caracteres", MinimumLength = 3)]
        public string ContactLastName { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Required(ErrorMessage = "Tem que introduzir um {0}")]
        [Display(Name = "Telefone")]
        [StringLength(30, ErrorMessage = "O campo {0} deverá conter entre {2} a {1} caracteres", MinimumLength = 9)]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Tem que introduzir uma {0}")]
        [Display(Name = "Morada")]
        [StringLength(100, ErrorMessage = "O campo {0} deverá conter entre {2} a {1} caracteres", MinimumLength = 10)]
        public string Address { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public virtual ICollection<SupplierProduct> SupplierProducts { get; set; } 
    }
}