﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace market.ViewModels
{
    public class marketContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public marketContext() : base("name=marketContext")
        {
        }

        // quando o modelo é criado, vamos desativar a opção delete cascade
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        public System.Data.Entity.DbSet<market.ViewModels.Product> Products { get; set; }

        public System.Data.Entity.DbSet<market.ViewModels.DocumentType> DocumentTypes { get; set; }

        public System.Data.Entity.DbSet<market.ViewModels.Employee> Employees { get; set; }

        public System.Data.Entity.DbSet<market.ViewModels.Supplier> Suppliers { get; set; }

        public System.Data.Entity.DbSet<market.ViewModels.Customer> Customers { get; set; }
    }
}
