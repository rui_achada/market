﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(market.Startup))]
namespace market
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
